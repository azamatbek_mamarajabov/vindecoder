
from django.urls import path, include
from . import views


urlpatterns = [
    path('api/<vin>', views.VinNumberAPIView.as_view(), name='apivinnumber'),
]

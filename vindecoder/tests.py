from django.test import TestCase
from .models import VinNumber
# Create your tests here.


class VinNumberTest(TestCase):

    def setUp(self):
        self.vinnumber = VinNumber.objects.create(
            vin="12121", year=1996,
            make="Toyota", model='model', color='color',
            type='type', dimentions='dimantions', weight='weight')

    def test_vinnumber_listing(self):
        self.assertEqual(f'{self.vinnumber.vin}', '12121')
        self.assertEqual(f'{self.vinnumber.year}', '1996')
        self.assertEqual(f'{self.vinnumber.make}', 'Toyota')
        self.assertEqual(f'{self.vinnumber.model}', 'model')
        self.assertEqual(f'{self.vinnumber.color}', 'color')
        self.assertEqual(f'{self.vinnumber.type}', 'type')
        self.assertEqual(f'{self.vinnumber.dimentions}', 'dimantions')
        self.assertEqual(f'{self.vinnumber.weight}', 'weight')

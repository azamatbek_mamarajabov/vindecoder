from django.contrib import admin

from .models import VinNumber
# Register your models here.


class VinNumberAdmin(admin.ModelAdmin):
    list_display = ("vin", "model", "year", "make",)


admin.site.register(VinNumber, VinNumberAdmin)

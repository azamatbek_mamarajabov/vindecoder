import datetime
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


def current_year():
    return datetime.date.today().year


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


class VinNumber(models.Model):
    vin = models.CharField(max_length=18, unique=True)
    year = models.PositiveIntegerField(
        validators=[MinValueValidator(1900), max_value_current_year])
    make = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    color = models.CharField(max_length=300, null=True,)
    type = models.CharField(max_length=100)
    dimentions = models.CharField(max_length=100)
    weight = models.CharField(max_length=100)

    def __str__(self):
        return self.vin
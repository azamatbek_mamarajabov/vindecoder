from rest_framework import serializers
from .models import VinNumber


class VinNumberSerializer(serializers.ModelSerializer):

    class Meta:
        model = VinNumber
        fields = '__all__'

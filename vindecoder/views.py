from django.shortcuts import render

import requests

from rest_framework.views import APIView
from rest_framework.response import Response

from .models import VinNumber
from .serializers import VinNumberSerializer
from .constants import VIN_DECODER, DETAILS, FORMAT


class VinNumberAPIView(APIView):
    queryset = VinNumber.objects.all()
    lookup_url_kwarg = "vin"

    def get(self, request, *args, **kwargs):
        vin = self.kwargs['vin']

        if VinNumber.objects.filter(vin=vin):
            """ if there is a vin in database fetch it"""

            vinnumber = VinNumber.objects.get(vin=vin)
            serializer = VinNumberSerializer(vinnumber)
            return Response(serializer.data)

        resp = get_decoded_vin(vin)

        result = vin_parse(resp)

        serializer = VinNumberSerializer(data=result, many=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response(serializer.data)


def get_decoded_vin(query):
    """This funtion is used for sending request to server to get decoded vin """

    results = {}
    url = VIN_DECODER+str(query)+'/'+DETAILS+'.'+FORMAT

    try:
        results = requests.get(url)

    except requests.ConnectionError as exception:
        return f'{exception}'
    return results


def vin_parse(query):
    """This funtion is used for making parsing on vin query"""

    json_all = query.json()

    make = json_all['make']['name']
    model = json_all['model']['name']
    year = json_all['years'][0]['year']
    vin = json_all['vin']
    dimentions = json_all['categories']['vehicleStyle']
    type = json_all['categories']['vehicleType']
    weight = json_all['categories']['vehicleSize']
    colorset = []
    if json_all['colors']:
        colorset = json_all['colors'][0]['options']
    colors = []
    if colorset:
        for i in colorset:
            colors.append(i['name'])

    result = [{'vin': vin, 'make': make, 'year': year, 'model': model, 'dimentions': dimentions,
               'weight': weight, 'type': type, 'color': str(colors)
               }]
    return result

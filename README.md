#   VIN Decoder

##  Installation

via docker-compose 


    docker-compose up -d --build
    docker-compose exec web python manage.py makemigrations
    docker-compose exec web python manage.py migrate


#

##  Usage

using urls given below you can use is properly

[**api/{vin}**](http://localhost:8000/api/)  is the main url where given {vin} is decoded and respond is given 
(**Caution:** because we are using moc server you can only use 3 vins `4T1BF1FK4FU954706 KNDUP131336382016 JHMCM563X7C022272` )

If you want to change server please find constants file in vindecoder app there you can change **VIN_DECODER** constant and change server url. And for extra changes go to views file and change `url` variable in `get_decoded_vin()` function, 

For api docs, go to
[**redoc/**](http://localhost:8000/redoc/)  


For api swagger, go to
[**swagger/**](http://localhost:8000/swagger/)  

To see swagger yaml 
[**swagger.yaml**](http://localhost:8000/swagger.yaml)  

To see swagger json 
[**swagger.json**](http://localhost:8000/swagger.json)  


####  Admin
To use admin please first create superuser
    
    docker-compose exec web python manage.py createsuperuser

admin url
[**admin**](http://localhost:8000/admin/)  

#

##  How it works

The main work is operated in *app:* vindecoder, *file:* views, *class:* VinNumberAPIView, *method:* get

First it gets vin via url args, then checks whether given vin excists in database, if  there is no matching vin it requests to server, fetchs the result and saves it to database and shows
if there is a matching vin it immidiately shows the result

